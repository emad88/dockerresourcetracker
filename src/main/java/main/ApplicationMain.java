package main;

import configuration.SpringConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import service.Service;


/**
 * Created by nikkhouy on 5.2.2016.
 */

public class ApplicationMain {


    public static void main(String[] args) throws Exception {

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
       // Service service = applicationContext.getBean("service",Service.class);
       // service.requestContainersDetails();
    }


}
