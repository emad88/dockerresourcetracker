package configuration;

import org.json.JSONObject;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import service.Service;

import java.util.ArrayList;
import java.util.Timer;


/**
 * Created by nikkhouy on 5.2.2016.
 */
//
@EnableScheduling
@Configuration
@ComponentScan("service,main")
@PropertySource("app.properties")
public class SpringConfiguration {

    @Bean
    public static PropertySourcesPlaceholderConfigurer getPropertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    @Scope("prototype")
    public ArrayList getArrayList(){
        return new ArrayList();
    }

    @Bean
    @Scope("prototype")
    public HttpHeaders getHttpHeaders() {
        return new HttpHeaders();
    }

    @Bean
    @Scope("prototype")
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Scope("prototype")
    public AsyncRestTemplate getAsyncRestTemplate(){
        return new AsyncRestTemplate();
    }

    @Bean
    @Scope("prototype")
    public JSONObject getJsonObject() {
        return new JSONObject();
    }

    @Bean
    @Scope("prototype")
    public Timer getTimer(){
        return new Timer();
    }
    @Bean(name = "service")
    public Service getService(){
        return new Service();
    }


}


