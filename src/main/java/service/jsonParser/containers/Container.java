package service.jsonParser.containers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import service.Service;
import service.jsonParser.containerStats.ContainerStats;
import service.logic.AverageResourceUsage;
import service.logic.CalcResourceUsage;
import service.restClient.docker.Docker;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by nikkhouy on 5.2.2016.
 */
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.PascalCaseStrategy.class)
public class Container {


    @Autowired
    private CalcResourceUsage calcResourcePercentage;
    @Autowired
    private AverageResourceUsage averageResourceUsage;
    @Autowired
    private Docker docker;
    private ContainerStats containerStats;
    private int index;
    @Autowired
    private Timer timer;
    private boolean canceled = false;


    public void startTimerTask() {
        timer.schedule(new TimerTask() {
            public void run() {
                if (docker != null) {
                    try {
                        docker.requestContainerStats(getId(), getIndex(), Container.this);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 0, 1000);

    }

    public void stopTimerTask() {
        canceled = true;
        timer.cancel();
        timer.purge();
    }

    public void setContainerStatsResponse(Service service) throws IOException {
        if (canceled == false)
            service.setContainerStatsResponse(this, index);

    }

    private String Id;

    private List<String> Names;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public List<String> getNames() {
        return Names;
    }

    public void setNames(List<String> names) {
        Names = names;
    }

    public CalcResourceUsage getCalcResourcePercentage() {
        return calcResourcePercentage;
    }

    public AverageResourceUsage getAverageResourceUsage() {
        return averageResourceUsage;
    }

    public ContainerStats getContainerStats() {
        return containerStats;
    }

    public void setContainerStats(ContainerStats containerStats) {
        this.containerStats = containerStats;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
