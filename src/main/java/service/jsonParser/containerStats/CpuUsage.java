package service.jsonParser.containerStats;

/**
 * Created by nikkhouy on 8.2.2016.
 */

public class CpuUsage {


    private long total_usage;

    public long getTotal_usage() {
        return total_usage;
    }

    public void setTotal_usage(long total_usage) {
        this.total_usage = total_usage;
    }
}
