package service.jsonParser.containerStats;


/**
 * Created by nikkhouy on 8.2.2016.
 */

public class CpuStats {


    private CpuUsage cpu_usage;

    private long system_cpu_usage;

    public long getSystem_cpu_usage() {
        return system_cpu_usage;
    }

    public void setSystem_cpu_usage(long system_cpu_usage) {
        this.system_cpu_usage = system_cpu_usage;
    }

    public CpuUsage getCpu_usage() {
        return cpu_usage;
    }

    public void setCpu_usage(CpuUsage cpu_usage) {
        this.cpu_usage = cpu_usage;
    }
}
