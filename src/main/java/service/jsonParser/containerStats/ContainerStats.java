package service.jsonParser.containerStats;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by nikkhouy on 8.2.2016.
 */
@Component
@Scope("prototype")
public class ContainerStats {


    private CpuStats cpu_stats;
    private MemoryStats memory_stats;

    public MemoryStats getMemory_stats() {
        return memory_stats;
    }

    public void setMemory_stats(MemoryStats memory_stats) {
        this.memory_stats = memory_stats;
    }

    public CpuStats getCpu_stats() {
        return cpu_stats;
    }

    public void setCpu_stats(CpuStats cpu_stats) {
        this.cpu_stats = cpu_stats;
    }
}
