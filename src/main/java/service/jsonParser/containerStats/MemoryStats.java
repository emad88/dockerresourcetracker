package service.jsonParser.containerStats;

/**
 * Created by nikkhouy on 10.2.2016.
 */
public class MemoryStats {

    private long usage;
    private long limit;

    public long getUsage() {
        return usage;
    }

    public void setUsage(long usage) {
        this.usage = usage;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }
}
