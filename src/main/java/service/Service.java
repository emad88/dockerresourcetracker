package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import service.jsonParser.containers.Container;
import service.logic.AverageResourceUsage;
import service.logic.CalcResourceUsage;
import service.restClient.docker.Docker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikkhouy on 8.2.2016.
 */
@Component
public class Service {

    @Autowired
    private Docker docker;
    private AverageResourceUsage averageResourceUsage;
    private CalcResourceUsage calcResourceUsage;
    @Autowired
    private ArrayList<Long> previousCpuTotalUsage;
    @Autowired
    private ArrayList<Long> previousCpuSystemUsage;
    @Autowired
    private ArrayList<Integer> counterCpu;
    @Autowired
    private ArrayList<Integer> counterMemory;
    @Autowired
    private ArrayList<Boolean> raiseAlertIsNotCalled;
    private Container[] containers;


    @Scheduled(fixedRate = 5000)
    public void requestContainersDetails() throws IOException {
        removeUnneededData();
        registerService();
        docker.requestContainersDetails();
    }

    private void registerService() {
        docker.registerService(this);
    }

    public void setContainersDetailsResponse(Container[] containers) {
        this.containers = containers;
        for (int i = 0; i < containers.length; i++) {
            initializeListElements();
        }
    }


    private void initializeListElements() {
        long zero = 0;
        previousCpuSystemUsage.add(zero);
        previousCpuTotalUsage.add(zero);
        counterCpu.add(0);
        counterMemory.add(0);
        raiseAlertIsNotCalled.add(true);
    }

    public void setContainerStatsResponse(Container containers, int index) throws IOException {
        calcResourceUsage = containers.getCalcResourcePercentage();
        averageResourceUsage = containers.getAverageResourceUsage();
        cpuUsagePercentage(containers, index);
        memoryUsagePercentage(containers, index);
    }

    private void cpuUsagePercentage(Container containers, int index) {
        long currentCpuTotalUsage = containers.getContainerStats().getCpu_stats().getCpu_usage().getTotal_usage();
        long currentCpuSystemUsage = containers.getContainerStats().getCpu_stats().getSystem_cpu_usage();
        calcResourceUsage.calculateCpuPercentage(currentCpuTotalUsage, currentCpuSystemUsage, previousCpuTotalUsage.get(index), previousCpuSystemUsage.get(index));
        previousCpuTotalUsage.set(index, currentCpuTotalUsage);
        previousCpuSystemUsage.set(index, currentCpuSystemUsage);
        averageResourceUsage.monitorCPU(containers, counterCpu, raiseAlertIsNotCalled, index);
    }

    private void memoryUsagePercentage(Container containers, int index) {
        long memoryLimits = containers.getContainerStats().getMemory_stats().getLimit();
        long memoryUsage = containers.getContainerStats().getMemory_stats().getUsage();
        calcResourceUsage.calculateMemoryPercentage(memoryLimits, memoryUsage);
        averageResourceUsage.monitorMemory(containers, counterMemory, raiseAlertIsNotCalled, index);
    }

    private void removeUnneededData() {
        if (containers != null)
            for (int i = previousCpuSystemUsage.size() - 1; i >= containers.length; i--) {
                previousCpuTotalUsage.remove(i);
                previousCpuSystemUsage.remove(i);
                counterCpu.remove(i);
                counterMemory.remove(i);
                raiseAlertIsNotCalled.remove(i);
            }


    }


}
