package service.restClient.monitoring;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;

/**
 * Created by nikkhouy on 15.2.2016.
 */
@Component
public class MonitoringSystem {

    @Value("${controllerAddress}")
    private String controllerAddress;
    @Autowired
    private JSONObject dockerInfo;
    @Autowired
    private HttpHeaders httpHeaders;
    @Autowired
    private AsyncRestTemplate restTemplate;
    private ListenableFuture<ResponseEntity<String>> future;

    public void sendNotification(String dockerName, double cpuPercentage, double memoryPercentage) {
        buildJsonForServer(dockerName, cpuPercentage, memoryPercentage);
        httpHeaders.set("Authorization", "Basic " + getUserPass());
        httpHeaders.set("Content-Type", "application/json");
        HttpEntity entity = new HttpEntity(dockerInfo.toString(), httpHeaders);
        future = restTemplate.exchange(controllerAddress + "/alert", HttpMethod.POST, entity, String.class);
        serverCallBack();
    }

    private void buildJsonForServer(String dockerName, double cpuPercentage, double memoryPercentage) {
        dockerInfo.put("dockerName", cutDockerName(dockerName));
        dockerInfo.put("cpuPercentage", cpuPercentage);
        dockerInfo.put("memoryPercentage", memoryPercentage);

    }

    private String cutDockerName(String dockerName){
        return dockerName.substring(6);
    }

    private String getUserPass() {
        return new String(Base64.encodeBase64(("admin" + ":" + "admin").getBytes()));
    }

    private void serverCallBack() {
        future.addCallback(
                new ListenableFutureCallback<ResponseEntity<String>>() {
                    @Override
                    public void onSuccess(ResponseEntity<String> response) {
                        System.out.println("RESPONSE: " + response.getBody());
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        System.out.println("Error inside NotifyController serverResponseCallback(): " + t.getMessage());
                    }
                }

        );
    }
}
