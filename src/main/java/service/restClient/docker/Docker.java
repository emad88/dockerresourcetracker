package service.restClient.docker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import service.Service;
import service.jsonParser.containerStats.ContainerStats;
import service.jsonParser.containers.Container;

import java.io.IOException;

/**
 * Created by nikkhouy on 5.2.2016.
 */
@Component
public class Docker {

    @Value("${dockerAddress}")
    private String address;
    private Service service;
    @Autowired
    private RestTemplate syncRestTemplate;
    @Autowired
    private AutowireCapableBeanFactory beanFactory;
    private Container[] containers;


    public void registerService(Service service) {
        this.service = service;
    }

    public void requestContainersDetails() throws IOException {
        stopTimerTasks(containers);
        containers = syncRestTemplate.getForObject(address + "json", Container[].class);
        containersDetails(containers);
    }

    public void requestContainerStats(String containerID, int index, Container container) throws IOException {
        ContainerStats containerStats = syncRestTemplate.getForObject(address + containerID + "/stats", ContainerStats.class);
        containerStats(containerStats, index, container);
    }

    private void containersDetails(Container[] containers) {
        setIndexes(containers);
        autowireDependencies(containers);
        startTimerTasks(containers);
        service.setContainersDetailsResponse(containers);
    }

    private void containerStats(ContainerStats containerStats, int index, Container container) throws IOException {
        containers[index].setContainerStats(containerStats);
        container.setContainerStatsResponse(service);

    }

    private void autowireDependencies(Container[] containers) {
        for (Container container : containers) {
            beanFactory.autowireBean(container);
        }
    }

    private void setIndexes(Container[] containers) {
        for (int i = 0; i < containers.length; i++) {
            containers[i].setIndex(i);
        }
    }

    private void startTimerTasks(Container[] containers) {
        for (int i = 0; i < containers.length; i++) {
            containers[i].startTimerTask();
        }
    }

    private void stopTimerTasks(Container[] containers) {
        if (containers != null) {
            for (int i = 0; i < containers.length; i++) {
                containers[i].stopTimerTask();
            }
        }
    }



}

