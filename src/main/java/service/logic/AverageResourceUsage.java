package service.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import service.jsonParser.containers.Container;
import service.restClient.monitoring.MonitoringSystem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikkhouy on 15.2.2016.
 */
@Component
@Scope("prototype")
public class AverageResourceUsage {

    @Value("${threshold}")
    private double THRESHOLD;
    private ArrayList<Boolean> raiseAlertIsNotCalled;
    private int count;
    @Autowired
    private MonitoringSystem monitoringSystem;



    public void monitorCPU(Container containers, ArrayList<Integer> counter, ArrayList<Boolean> raiseAlertIsNotCalled, int index) {
        this.raiseAlertIsNotCalled = raiseAlertIsNotCalled;
        count = counter.get(index);
        double cpuUsage = containers.getCalcResourcePercentage().getCpuPercentage();
        double memoryUsage = containers.getCalcResourcePercentage().getMemoryPercentage();
        String containerName = containers.getNames().get(0);
        if (cpuUsage > THRESHOLD || memoryUsage > THRESHOLD) {
            counter.set(index, count+1);
            System.out.println("container name: "+ containerName + " counter= " + counter.get(index));
            if (counter.get(index) > 10 && this.raiseAlertIsNotCalled.get(index)) {
                raiseAlert(containers,index);
            }
        } else {
            counter.set(index, 0);
        }
    }

    public void monitorMemory(Container containers, ArrayList<Integer> counter, ArrayList<Boolean> raiseAlertIsNotCalled, int index){
        monitorCPU(containers, counter, raiseAlertIsNotCalled, index);
    }

    private void raiseAlert(Container containers,int index) {
        raiseAlertIsNotCalled.set(index, false);
        monitoringSystem.sendNotification(containers.getNames().get(0), containers.getCalcResourcePercentage().getCpuPercentage(), containers.getCalcResourcePercentage().getMemoryPercentage());

    }

}
