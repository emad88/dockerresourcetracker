package service.logic;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by nikkhouy on 11.2.2016.
 */
@Component
@Scope("prototype")
public class CalcResourceUsage {

    private double memoryPercentage;
    private double cpuPercentage;


    public void calculateCpuPercentage(long currentCpuTotalUsage, long currentCpuSystemUsage, long previousCpuTotalUsage, long previousCpuSystemUsage) {
        cpuPercentage = round((cpuDelta(currentCpuTotalUsage, previousCpuTotalUsage) / (double) systemDelta(currentCpuSystemUsage, previousCpuSystemUsage)) * 100);
    }

    public void calculateMemoryPercentage(long memoryLimits, long memoryUsage) {
        memoryPercentage = round((memoryUsage / (double) memoryLimits) * 100);
    }

    public double getMemoryPercentage() {
        return memoryPercentage;
    }

    public double getCpuPercentage() {
        return cpuPercentage;
    }

    private double round(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    private long cpuDelta(long currentCpuTotalUsage, long previousCpuTotalUsage) {
        return currentCpuTotalUsage - previousCpuTotalUsage;
    }

    private long systemDelta(long currentCpuSystemUsage, long previousCpuSystemUsage) {
        return currentCpuSystemUsage - previousCpuSystemUsage;
    }


}
